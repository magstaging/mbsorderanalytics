1. clone the repository

2. create folder app/code/Mbs/Order when located at the root of the Magento site

3. copy the content of this repository within the folder app/code/Mbs/Order

4. install the module php bin/magento setup:upgrade

5. create a Magento custom theme and copy the file (vendor/magento/module-checkout/view/frontend/templates/success.phtml)
 to app/design/frontend/<Vendor>/<theme>/Magento_Checkout/templates/success.phtml
 
 append your analytics to this file: for instance below is ceneo analitycs
 
 <?php
 /** @var \Mbs\Order\ViewModel\Analytics $ceneoAnalyticsViewModel */
 $ceneoAnalyticsViewModel = $block->getData('analyticsViewModel');  ?>
 
 <script type="text/javascript">
 
     ceneo_client_email = '<?= $ceneoAnalyticsViewModel->getCustomerEmailFromOrder(); ?>
 
     ceneo_order_id = '<?= $block->escapeHtml($block->getOrderId()); ?>';
 
     ceneo_shop_product_ids ='<?= $ceneoAnalyticsViewModel->getProductIdsFromOrder(); ?>';
 
     ceneo_work_days_to_send_questionnaire = 3;
 
 </script>
 
 <script type="text/javascript" src="https://ssl.ceneo.pl/transactions/track/v2/script.js?accountGuid=XXXXXXXX">
 
 </script>

5. place an order and verify the order success page render the expected analytics code 




