<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 01/09/2018
 * Time: 19:30
 */

namespace Mbs\Order\ViewModel;

use Magento\Sales\Model\Order;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Analytics implements ArgumentInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * @var Order
     */
    private $lastRealOrder;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->lastRealOrder = $this->checkoutSession->getLastRealOrder();
    }

    /**
     * @param Order $order
     * @return array
     */
    public function getProductIdsFromOrder()
    {
        $productIds = [];
        foreach ($this->lastRealOrder->getAllItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            $productIds[] = $item->getProductId();
        }

        return implode(',', $productIds);
    }

    /**
     * @return string
     */
    public function getCustomerEmailFromOrder()
    {
        return $this->lastRealOrder->getCustomerEmail();
    }

}